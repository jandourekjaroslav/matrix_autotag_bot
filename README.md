# Matrix_autotag_bot
based on : [matrix-nio](https://github.com/poljar/matrix-nio)

Hard Alpha use at own peril
## Usage manual for auto_tag_bot

#### Use `!autotag_help` to print this.
---
#### Use `!autotag_list` to print configured autotags
---
#### Use `!autotag_add` to to add an autotag with the following allowed syntax:

`/plain !autotag_add name regex tag_list room_id`

Where:
- name - unique name for autotag rule
- regex - is a reqular expresion without quotes i.e. ^word.+$
- tag_list - is a a comma separated list of user to tag on match i.e @user1:server1.org,@user2:server2.com
- room_id - is an id of a room where you want autotagging to fire, autotag_bot and at least one user from the - tag_list must be present in the room for this to work.
- /plain - should be used always but is required only if using characters interpreted by markdown

## Publicly available bot
If you feel like trying it out feel free to invite this bot. __*0 guarantee of uptime, sorry*__  ¯\\\_(°n°)\_/¯

**[@autotag_bot:chat.traktoristi.cz](https://matrix.to/#/@autotag-bot:chat.traktoristi.cz)**

Avaiable only on the main Federation cluster 

**FOR TEST PURPOSES ONLY, ALL DATA (configured autotags) STORED IN PLAINTEXT, READABLE BY ANYBODY, NO WAY TO DELETE**